#import "fa.typ": *

#let cvtitle = [
    Sil Vaes
]

#let cvsubtitle = [
    R&D Software Engineer
]

#let links = (
    (icon: fa-envelope(), link: "mailto:sil.g.vaes@gmail.com", display: smallcaps("sil.g.vaes@gmail.com")),
    // (icon: "website", link: "https://mattrighetti.com/", display: "mattrighetti.com"),
    //(icon: "github", link: "https://github.com/mattrighetti", display: "mattrighetti"),
    (icon: fa-linkedin(), link: "https://www.linkedin.com/in/silvaes/", display: smallcaps("silvaes")),
    (icon: fa-phone(), display: smallcaps("(+32) 499315522")),
)


#let link_bar(linkarray) = {
    // set the text after this statement to be 8pt in size
    set text(gray)

    // iterate over the array
    // this is the equivalent of a for loop
    let newarray = linkarray.map(l => {
        // render the icon using the previous template
        l.icon
        " "
        // render the link text
        if "display" in l.keys() and "link" in l.keys() {
            // this is the equivalent of [$l.display](l.link) in markdown
            // you'll see square brackets a lot in typst, they are used as text
            // containers
            link(l.link)[#{l.display}]
        } else if "link" in l.keys() {
            // my email is the only field that does not have a display field
            // but the link function will automatically display what's after `mailto:`
            link(l.link)
        } else {
            l.display
        }
    })

    grid(
        columns: (33%, 33%, 33%),
        column-gutter: 0pt,
        align(left, newarray.at(0)),
        align(center, newarray.at(1)),
        align(right, newarray.at(2)),
    )
}

#let margin = 2cm
#let accent_color = gray
#let darkgray = luma(100)
#let line_stroke = (paint: darkgray, thickness: 2pt, cap: "round")
#let header_stroke = (paint: gray, thickness: 1pt, cap: "round")

#let entry(title, employer, loc, body, date) = [
    #heading(level: 2, title)

    #grid(
        columns: (50%, 50%),
        column-gutter: 0pt,
        row-gutter: 5pt,
        text(fill: darkgray, employer),
        align(right, text(fill: gray, loc)),
        text(size: 8pt, fill: darkgray, body),
        align(right, text(fill: gray, date)),
    )
]

#show heading.where(
  level: 1
): it => text(
    size: 14pt,
    weight: "bold",
    // style: "oblique",
    smallcaps(it.body) + [#linebreak()],
)

#show heading.where(
  level: 2
): it => text(
    size: 10pt,
    weight: "bold",
    // style: "oblique",
    it.body + [#linebreak()],
)

#set text(
    font: "Roboto",
    weight: "light",
    size: 10pt
)
#set page(
    //background: place(top + right, rect(fill: accent_color.lighten(80%), width: 33.33333%, height: 100%)),
    paper: "a4",
    margin: (x: margin, y: margin),
    footer: [#link_bar(links)]
    // header: align(right + horizon, title),
)

// #set block(above: 0pt, below: 0pt)

#set par(
    justify: true,
    leading: 0.50em,
    first-line-indent: 0pt,
    hanging-indent: 0pt,
)

#let cvheader(title, subtitle) = grid(
    columns: (5%, 90%),
    rows: (20pt, 20pt),
    line(stroke:  line_stroke, angle: 90deg, length: 30pt, start: (0pt, -10pt)),
    align(left, text(18pt)[
        *#title*
    ]),
    line(stroke:  line_stroke, angle: 90deg, length: 20pt),
    align(left, text(gray, 14pt)[
        *#subtitle*
    ]),
    line(stroke:  line_stroke, angle: 0deg, length: 100%),
    line(stroke:  line_stroke, angle: 0deg, length: 35% ) + line(stroke:  line_stroke, angle: 90deg, start: (35.09%, -12pt), length: 22pt)
)

#cvheader(cvtitle, cvsubtitle)
#v(0pt, weak: true) 
#let about_me = [
    #v(5pt, weak: true) 
    = About me
    #set text(size: 8pt)
    As a recent computer science graduate with a first job experience, I bring a keen inquisitive nature and a deep interest in various aspects of the field.
    
    My academic journey has been marked by a strong focus on programming languages and the theoretical underpinnings of computer science.
    I am enthusiastic about exploring and utilizing new technologies when the need arises, always seeking to stay up-to-date of the latest developments.
    
    Additionally, I am a proponent and user of open source software, which aligns with my passion for collaborative and innovative computing solutions.
]

#let programming = [
    = Programming

    - C++
    - Python
    - Rust
    - OOP
    - Functional programming
]

#let tools = [
    = Tools

    - Linux
    - Nix
    - Git
    - Build systems
    - Basic DevOps tools
    - Basic Kubernetes
    - Basic containers
    - Terraform
    - PostgreSQL
    - PyTorch/Keras/Tensorflow
]

#let languages = [
    = Languages

    - Dutch (native)
    - English (fluent)
    - French (basic)
]

#let experience = [
    #v(5pt, weak: true)
    = Experience
    #entry([R&D Software Engineer], [RideOnTrack], "Geel, Belgium",
        [C++ developer working on dispatcher software.], "Aug. 2023")
    #entry([Master's Thesis], [Hasselt University], "Diepenbeek, Belgium",
        [A comparison of blockchain development languages. Including Script, Solidity, Plutus, and Aiken.], "Sep. 2022 - Jun. 2023")
    #entry([Summer of Nix], [Nix Foundation], "",
        [A two month train and work program for Nix.], "Jul. 2021 - Sep. 2021")
    #entry([Bachelor's Thesis], [Hasselt University], "Diepenbeek, Belgium",
        [Comparison of modern C++ with contemporary programming language features.], "Jan. 2021 - Jul. 2021")
    #entry([C++ Software Developer], [Optimum Sorting], "Hasselt, Belgium",
        [C++ developer creating a frontend and backend for sorting machings.], "Jul. 2020 - Sep. 2020")
    #entry([Bachelor's Thesis], [PXL College], "Hasselt, Belgium",
        [Developed a hard- and software platform for predicting laser sensor failure.], "Jan. 2020 - Jul. 2020")
    #entry([Internship], [KUKA], "Houthalen, Belgium",
        [Integrating a 2D perimeter measuring laser and industrial robot to accurately pick up objects.], "Aug. 2015 - Dec. 2015")
    #entry([ASML Project], [TU/e], "Eindhoven, Belgium",
        [Mapping and documenting hard- and software for an ASML device.], "Aug. 2014 - Jun. 2015")
    
]

#let education = [
    = Education
    #entry([MSc. in Informatics], [Hasselt University], "Diepenbeek, Belgium",
        [Achieved my master's degree in computer science with a specialization in AI/ML.], "2020 - 2023")
    #entry([Bachelor's degree in Electronics-ICT], [PXL College], "Hasselt, Belgium",
        [Achieved my bachelor's degree magna cum laude.], "2017 - 2020")
    #entry([BSc. in Mechatronics], [Fontys College], "Eindhoven, Netherlands",
        [Achieved 148 credits and switched because electronics-ICT was more in line with my area of interest.], "2012 - 2017")
]

#let body = experience + education
#let side = about_me + programming + tools + languages

#grid(
    columns: (34.5%, 1%, 64.5%),
    column-gutter: 10pt,
    row-gutter: 0pt,
    block(outset: 0pt, inset: (top: 0cm, right: 0cm, rest: 0cm), stroke: none, width: 100%, {
        set block(above: 10pt)
        show heading.where(level: 1): it => style(s => {
            let dim = measure(it, s)
            stack(
                dir: ltr,
                align(left, it),
                place(
                    dy: 6pt,
                    dx: 5pt,
                    horizon + left,
                    line(stroke: header_stroke, length: 100% - dim.width - 5pt),
                ),
            )
        })
        side
    }),
    line(stroke:  line_stroke, angle: 90deg, length: 91%),
    block(outset: 0pt, inset: (top: 0cm, right: 0cm, rest: 0cm), stroke: none, width: 100%, {
        set block(above: 10pt)
        show heading.where(level: 1): it => style(s => {
            let dim = measure(it, s)
            stack(
                dir: ltr,
                place(
                    dy: 6pt,
                    dx: 0pt,
                    horizon + left,
                    line(stroke: header_stroke, length: 100% - dim.width - 5pt),
                ),
                align(right, it),
            )
        })
        body
    }),
    // experience,
)
